---
title: An omen of Nine Chapters (00X)
date: 2019-XX-XX 01:00:00
tag:
   - archive
   - omen
   - project name (if needed)
---

> While you're turning the pages of the Nine Chapters,
> <another stupid sentence>

## Updated

Another week has been passed since [the last _omen_](https://9chapters.gitlab.io/devlog/2019/05/22/006_omen001/). We're happy to update with you ...

+ <project 1>
  + <updated item 1>
  + <updated item 2>
+ <project 2>
  + <updated item 1>
  + <updated item 2>

## Upcoming

<another blah-blah-blah>

+ <project 1>
  + <item 1>
  + <item 2>

+ <project 2>
  + <item 1>
  + <item 2>
