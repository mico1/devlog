# Nine Chapters' Devlog

This is the repository for Nine Chapters' devlog hosted on gitlab.

## how to write an omen (devlog)

### create a new post

Firstly, copy `omen_template.md` on repository root to /`source/_posts/` and rename it as following format:

```
[post-id]_omen[omen-id].md
```

for example, `007_omen002.md`.

### how to insert an image

Let say you want to insert an image `abc.png`. There are two necessary steps:

+ put `abc.png` into `/source/images/`
+ then, you just need to paste `![](/devlog/images/abd.png)` into you markdown file.

Please notice that two pathes are different.
