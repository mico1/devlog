
## general overview

The following illustrator is our roadmap in second half of Q2 2019. Updated on 22 May 2019.

![](../images/roadmap_20190601.png)

---

## chronological

+ **25 May**
   + [x] [dough] TzName specific version
   + [x] [identity][Q/A-sig] poc
+ **30 May**
   + [x] [dough] low-to-middle-profile release announcement
   + [x] [identity][Q/A-sig] poc test
+ **05 June**
   + [ ] [dough][data] general version of data translator/generator
   + [x] [TzResource][TzName] all functionalities develop
+ **10 June**
   + [x] [TzResource][TzName] PoC test
+ **15 June**
   + [ ] [TzResource][TzName] PoC demo
   + [ ] [TzResource][BakingPool] PoC dev and test
   + [ ] [TzResource] post for demo
+ **20 June**
   + [ ] [dough][deploy/invoke] general version of contract manipulator
   + [ ] [identity] support hardware wallet
   + [ ] [hov] testing and resumingx
+ **25 June**
+ **30 June**
   + [ ] avatar = public key + committee


--
