---
title: Rules upon a Commission
date: 2019-04-22 21:30:00
tag:
   - identity
   - hov
   - commission
   - archive
---

## The center commisson

To setup and deploy a center commission is not a hard task. To do a flexible one, however, will be entire different story.

### As an implementation of HOV

It's nature we demand a mechanism for _updating_ rule definition once we deloy a rule-base onto our _commission_.

The problem is, commission provides the service of [_voting_](https://tezos-southeast-asia.gitlab.io/identity-devlog/2019/04/08/commi-star/#voting-and-HOV), yet, to update rule on commisson actually dependents on another action of voting. Which means, commisson itself must be deployed along with its own [**bridge**](https://tezos-southeast-asia.gitlab.io/identity-devlog/2019/04/18/bridge/) contract.

And more important, the updating process need to be break down into two part: **pre-concensus** and **post-concensus**. Each of which can be defined as an independent contrac entry. If you have some programming background, this is pretty much like handling continuation by hand.

```mermaid
graph TB
   style n0 fill:#AAA

   n3(VotingBooth)
   subgraph hov
      subgraph committee
         n0(some member)
      end
      subgraph contract: Commission
         n1(entry: apply)
         n2(entry: ruleUpdate)
         n5(entry: ruleUpdateK)
      end
      n4(bridge)
   end

   n0 --> |request updating| n2
   n2 --> |self call| n1
   n1 ==> |originate| n3
   n3 --> |inform| n4
   n4 --> |trigger| n5
```

### Beyond Human

So far, the commission contract is around 50% completed.

+ [x] voting
   + [x] voting booth
+ [x] rulebase
   + [x] rule updating (continuation via voting booth)
   + [ ] bridge4RuleUpdate
+ [ ] membership
   + [ ] membership updating
   + [ ] bridge4MemberUpdate

But its signature is already as sick as leftovers. It's so crazy that I have to show the current commission contract (as in Michelson) here:

```
parameter
  (or :_entries
     (pair %_Liq_entry_apply string (pair (map address unit) (pair address key_hash)))
     (or (pair %_Liq_entry_rUpdate
            string
            (pair (option
                     (lambda
                        (map address (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative))))
                        (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative)))))
                  key_hash))
         (or (bool %_Liq_entry_rUpdateK) (address %_Liq_entry_updateRuleBridgeAddress))));
```

```
storage
  (pair :storage
     (map %committee address unit)
     (pair (string %committeeRule)
           (pair (list %apps address)
                 (pair (map %ruleBook
                          string
                          (lambda
                             (map address (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative))))
                             (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative)))))
                       (pair (address %ruleBridge)
                             (option %ruleLock
                                (pair string
                                      (option
                                         (lambda
                                            (map address (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative))))
                                            (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative))))))))))));
```

More detail can be found in [<i class="fab fa-gitlab"></i>identity](https://gitlab.com/tezos-southeast-asia/identity/tree/1a0f442616106e6522020f5af41cae7af9575bb3).
