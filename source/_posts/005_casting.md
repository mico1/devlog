---
title: The address, key and Key_hash
date: 2019-05-14 22:00:00
tag:
   - archive
---

## casting from key to address

command:

```bash
> tezos-client -A node1.lax.tezos.org.sg run script ./key2addr.tz on storage 'None' and input '"edpkuesJWtpYgjM1wjxHn7oiJsT64rMrGXztoa7eQ9MGRCL2GtEm21"'
```

output:

```bash
storage
  (Some (Pair "edpkuesJWtpYgjM1wjxHn7oiJsT64rMrGXztoa7eQ9MGRCL2GtEm21"
              "tz1bhXKVY4ihH8Dcao4cuk8KxJ4sPjXGZcEp"))
emitted operations
```

contract (as in Liquidity):

```ocaml
let key2addr (k : key) : address =
   let kh = Crypto.hash_key k in
   let inc = Account.default kh in
   let addr = Contract.address inc in
   addr

type storage = (key * address) option

let%entry main (newK : key) _ =
   let newAddr = (key2addr newK) in
   let s = Some (newK , newAddr) in
   ([] : operation list) , s
```

contract (as in Michelson):

```ocaml
parameter key;
storage (option (pair key address));
code { DUP ;
       DIP { CDR @__slash_1 } ;
       DIP { DROP } ;
       CAR @newK_slash_2 ;
       DUP ;
       HASH_KEY @kh ;
       IMPLICIT_ACCOUNT @inc ;
       ADDRESS @addr ;
       SWAP ;
       PAIR ;
       SOME @s ;
       NIL operation ;
       PAIR };
```
