---
title: About Here
---

Nine Chapters dedicates to finding the most general methods of solving problems. We apply _functional programming_, _formal verification_, _type theory_, _theorem proving_, _static analysis_, _DSL_ to make flexible, reliable and secured industrial decentralized application.

This website is Nine Chapters' devlog. Please feel free to contact us if you got any suggestion or question!

+ [official website](https://www.9chapters.io/)
+ [past events](https://www.9chapters.io/events/)
+ [medium](https://medium.com/9chapters)
+ [twitter](https://twitter.com/NineChaptersTW)
